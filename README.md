# WebdavCli - example of webdav upload functionality


## What was done

- Uploading files to remote webdav server
- Files are handled as streams, it can handle large files upload
- Supports cookies

## What is Missing

### Testing

The upload functionality was designed specifically to work with _comjoo-data_ server where the other libraries like _webdav-fs_ were not working properly. (actually the _webdav-fs_ and _webdav-client_ were working fine with _owncloud demo_, but not with _comjoo-data_).

The proper testing should consist of following steps:

- file is uploaded
- uploaded file has the right size
- custom headers are applied correctly
- cookies are saved correctly
- save the cookies and restore them for new session

Since _comjoo-data_ server acts differently then then even _owncloud-demo_ server, it might we worth of consideration to create an test account on _comjoo-data_ server.
