const fs = require('fs');
const path = require('path');
const axios = require('axios');
const tough = require('tough-cookie');

/** An Example webdav client implementation */
const webdavCli = class {
  /**
   *  Creates a new webdav client implementation 
   *  @param {String} url - The url of a webdav server root. Must include valid protocol (http/https)
   *  @param {Object} options - Additional options
   *
   *  @namespace options
   *  @prop {string} username - a username used for http authentication
   *  @prop {string} password - a password used for http authetication
   *  @prop {object} headers - additional headers to be sent with every query
   *  @prop {object} maxSize - max allowed filesize for upload 
   */
  constructor(url, options) {
    this.url = url;
    let axiosConf = {
      baseURL: this.url,
      headers: {
        'user-agent': 'Axios based webdav uploader',
        'accept': 'application/json'
      }
    }

    if (options.username && options.password) {
      axiosConf.auth = {
        username: options.username,
        password: options.password
      }
    }
    if (typeof options.headers === 'object') {
      axiosConf.headers = Object.assign({}, axiosConf.headers, options.headers); 
    }

    if (Number.isInteger(options.maxSize)) {
      axiosConf.maxContentLength = options.maxSize
    }
    this.axios = axios.create(axiosConf);
    this.cookieJar = new tough.CookieJar();
  }

  /**
   * Push a file to webdav server
   * @async
   * @param {String} localPath - The path to file which si going to be uploaded
   * @param {String} remotePath - The path of a new file on a webdav server, including the file name itself.
   * @example
   * //uploads the file from home directory into webdavSubfolder
   * webdavClient.upload('/home/jon.doe/myfile.pdf', '/webdavSubfolder/myfile.pdf')
  */
  async upload(localPath,remotePath) {
    if (!fs.existsSync(localPath)) throw (new Error("Local file does not exists"));
    if (!remotePath.startsWith('/')) remotePath = `/${remotePath}`;
    let cookies = this.cookieJar.getCookieStringSync(this.url);
    let headers = {
      'content-length': String(fs.statSync(localPath).size)
    }
    if (cookies) { headers.cookie = cookies}
    try {
      let res = await this.axios(
        {
          method: 'PUT',
          url: `${encodeURI(remotePath)}`,
          data: fs.createReadStream(localPath),
          headers
        }
      );
      let cookies = res.headers['set-cookie'];
      if (typeof cookies === 'string') cookies = [cookies];
      if (Array.isArray(cookies)) {
        cookies.forEach((cookie) => {
          this.cookieJar.setCookieSync(tough.Cookie.parse(cookie),this.url);
        })
      }
      return true;
    } catch(e) {
      if (e.response) {
        throw new Error(`${e.response.status} ${e.response.statusText}`);
      } else {
        throw new Error('Connection failed');
      }
    }
  }
  
  /**
   *  get session cookies. Useful for saving cookies after the session is over and instance is going to be destroyed
   *  @return {Object} cookies
  */
  getCookies() {
    return this.cookieJar.toJSON();
  }
  
  /**
   * set the cookies list. Useful for loading cookies from previous instance
   *  @param {Object} cookies - cookies object retrieved previously from getCookies method
   */
  setCookies(cookies) {
    this.cookieJar.fromJSON(cookies)
  }
}

module.exports = webdavCli;
