const path = require('path');
const fs = require('fs');
const process = require('process');
const Webdav = require('../index');


const url = process.env.WEBDAV_URL || "https://demo.owncloud.com/remote.php/dav/files/demo/"
const username = process.env.WEBDAV_USERNAME || "demo";
const password = process.env.WEBDAV_PASSWORD || "demo";

const webdav = new Webdav(url, 
  {
    username,
    password,
    headers: {
      'user-agent': 'Custom webdav uploader'
    },
    maxSize: 1024*1024*50
  }
)

async function main() {
  const filesPath = `${path.resolve(__dirname)}/test-files`
  let files = fs.readdirSync(filesPath);
  for (let i in files) {
    let file = files[i];
    try {
      let res = await webdav.upload(`${filesPath}/${file}`, file);
    } catch(e) {
      console.error(`can't upload file ${file}`)
      console.dir(e);
    }
  }
}

main();
